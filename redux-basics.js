const redux = require('redux');
const createStore = redux.createStore;
const initialState = {
  counter: 0
}
// Reducer
const rootReducer = (state = initialState, action) => {
  if(action.type === 'INC_COUNTER') {
    return {
      ...state,
      counter: state.counter + 1
    }
  }
  if(action.type === 'DESC_COUNTER') {
    return {
      ...state,
      counter: state.counter - action.value
    }
  }
  if(action.type === 'ADD_COUNTER') {
    return {
      ...state,
      counter: state.counter + action.value
    }
  }
  return state;
}
// Store
const store = createStore(rootReducer);
console.log(store.getState())
// Subscription
store.subscribe(() => {
  console.log('[Subs]', store.getState())
})
// Dispatching Action
store.dispatch({type: 'INC_COUNTER'});
store.dispatch({type: 'DESC_COUNTER', value: 3});
store.dispatch({type: 'ADD_COUNTER', value: 10})
console.log(store.getState())
